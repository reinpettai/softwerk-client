import React from 'react';

interface Props {
    title: string;
    message: string;
    hide: () => void;
}

const Notification: React.FC<Props> = ({title, message, hide}) => {

    return(
        <div onClick={hide} className="notification">
            <div className="background"></div>
            <div className="inner">
                <div className="title">{ title }</div>
                <div className="message">{ message }</div>
            </div>
        </div>
    );
};

export default Notification;
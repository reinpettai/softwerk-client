import React, { useState, useEffect, useRef } from 'react';
import AddProjectItemModal from '../AddProjectItemModal/AddProjectItemModal';
import Project from '../../interfaces/project';
import Item from '../../interfaces/item';
import ProjectListItem from './ProjectListItem/ProjectListItem';
import Notification from '../Notification/Notification';

interface Props {
    project: Project;
}

const ProjectItemList: React.FC<Props> = ({project}) => {

    const [show, setShow] = useState(false);
    const [list, setList] = useState<any[]>([]);
    const [selectedItems, setSelectedItems] = useState<number[]>([]);
    const [periods, setPeriods] = useState<any>([]);
    const [year, setYear] = useState(new Date().getFullYear());
    const [searchParam, setSearchParam] = useState<string>();
    const [noResults, setNoResults] = useState<boolean>(false);
    const [totalPages, setTotalPages] = useState<number>(1);
    const [showNotification, setShowNotification] = useState<boolean>(false);
    const [notification, setNotification] = useState({
        title: '',
        message: ''
    });

    let observer: any = useRef<IntersectionObserver>(null);
    let currentPage: any = useRef<number>(1);

    let listRef: any = useRef(null);

    const showModal = () => setShow(true);
    const closeModal = async (itemId: any) => {
        setShow(false);
        await getItem(itemId);
    };

    const getData = async (page = 1) => {

        if (searchParam && searchParam !== '') return; 

        currentPage.current = page;

        try {
            const res = await fetch(process.env.REACT_APP_API_URL+ 'items/' + project.projectId + '/' + year + '/' + page);
            const json = await res.json();

            if (page <= 1) {
                setTotalPages(json.result.totalPages);
                setList([]);
                setList(json.result.items);
            }
            else setList([...list, ...json.result.items]);
        }
        catch(err) {

        }

    };

    const getItem = async (itemId: any) => {

        try {
            const res = await fetch(process.env.REACT_APP_API_URL + 'items/' + itemId + '/' + year);
            const json: Item = await res.json();

            if (!json) return;
            setList([...list, json]);
            createNotification('Hankeobjekt lisatud', '');
        }
        catch(err) {
            
        }
    };

    const search = async () => {

        if (!searchParam || searchParam === '') {
            setNoResults(false);
            return getData(1);
        };
        setSelectedItems([]);

        try {
            const res = await fetch(process.env.REACT_APP_API_URL + 'items/' + project.projectId + '/' + year + '/search/' + searchParam);
            const json = await res.json();

            const itemCount = json.length;

            if (!itemCount || itemCount <= 0) setNoResults(true);

            setList([]);
            setList(json);
        }
        catch(err) {
            
        }
    }; 

    const handleCopy = async () => {

        if (!selectedItems || selectedItems.length < 1) return;

        try {
            const res = await fetch(process.env.REACT_APP_API_URL + 'items/copy', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(selectedItems)
            });

            const json: number[] = await res.json();

            if (json && json.length > 0) {
                createNotification('Read edukalt kopeeritud', 'Kopeeritud read: ' + selectedItems);
                setSelectedItems([]);
                getData();
            } 
        }
        catch(err) {

        }
    };

    const deleteItems = async () => {

        if (!selectedItems || selectedItems.length < 1) return;
        
        try {
            const res = await fetch(process.env.REACT_APP_API_URL + 'items/remove', {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(selectedItems)
            });

            const content = await res.json();
            
            if (content) { 
                getData();
                createNotification('Read edukalt kustutatud', 'Kustutatud read: ' + selectedItems);
                setSelectedItems([]);
            }
        }
        catch(err) {

        }

    };

    const handleListSelect = (id: number, selected: boolean) => {
        
        if (selected) {
            setSelectedItems((prev: number[]) => ([...prev, id]));
            return;
        }
        const index = selectedItems.indexOf(id);
        selectedItems.splice(index, 1);
    };

    const createNotification = async (title: string, message: string) => {

        setNotification({
            title: title,
            message: message
        });
        setShowNotification(true);
        setTimeout(hideNotification, 1000);
    };

    const hideNotification = async () => {

        setShowNotification(false);
        setNotification({
            title: '',
            message: ''
        });
    };

    /**
     * Period selection
     */

    const prevPeriod = () => setYear(year-5);
    const nextPeriod = () => setYear(year+5);

    useEffect(() => {

        const setSelectedPeriods = () => {

            const newPeriods: any = [];
    
            for (var i = year; i < year+5; i++) {
                newPeriods.push(i);
            }
    
            setPeriods(newPeriods);
            getData(1);
        };

        setSelectedPeriods();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [year]);
    
    /**
     * IntersectionObserver
     */

    useEffect(() => {

        if (observer && observer.current) observer.current.disconnect();
        if ((!listRef || !listRef.current) || 
            (searchParam && searchParam !== '') ||
            (totalPages === currentPage.current)) return;

        observer.current = new IntersectionObserver((entries: IntersectionObserverEntry[], observer: IntersectionObserverInit) => {
            const ratio = entries[0].intersectionRatio;

            if (ratio > 0) {
                const page = currentPage.current + 1;
                getData(page);
            }

        }, {});

        observer.current.observe(listRef.current);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [list]);

    return(
        <div className="project-items-list">
            <h3>{ project.name }</h3>
            <div className="actions">
                <div className="searchbox">
                    <input type="text" onChange={(event: any) => {
                        setSearchParam(event.target.value);
                    }} />
                    <button onClick={() => {
                        search();
                    }}>Otsi</button>
                </div>
                <button onClick={showModal}>Lisa rida</button>
                <button onClick={handleCopy}>Kopeeri rida</button>
                <button onClick={deleteItems}>Kustuta rida</button>
            </div>
            <header>
                <div className="code">Kood</div>
                <div className="object">Hankeobjekt</div>
                <ul className="periods">
                    { periods.map((item: any) => {
                        if (!item) return [];
                        return(<li key={'year-'+item}>
                            <div className="year">{item}</div>
                            <div className="period-info">
                                <div className="amount">Kogus</div>
                                <div className="price">Hind</div>
                                <div className="sum">Summa</div>
                            </div>
                        </li>);
                    }) }
                </ul>
                <div className="year-selection">
                    <button className="prev-btn" onClick={prevPeriod}>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 492 492" xmlSpace="preserve">
                                    <path d="M198.608,246.104L382.664,62.04c5.068-5.056,7.856-11.816,7.856-19.024c0-7.212-2.788-13.968-7.856-19.032l-16.128-16.12
                                        C361.476,2.792,354.712,0,347.504,0s-13.964,2.792-19.028,7.864L109.328,227.008c-5.084,5.08-7.868,11.868-7.848,19.084
                                        c-0.02,7.248,2.76,14.028,7.848,19.112l218.944,218.932c5.064,5.072,11.82,7.864,19.032,7.864c7.208,0,13.964-2.792,19.032-7.864
                                        l16.124-16.12c10.492-10.492,10.492-27.572,0-38.06L198.608,246.104z"/>
                        </svg>
                    </button>
                    <button className="next-btn" onClick={nextPeriod}>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 492.004 492.004" xmlSpace="preserve">
                                <path d="M382.678,226.804L163.73,7.86C158.666,2.792,151.906,0,144.698,0s-13.968,2.792-19.032,7.86l-16.124,16.12
                                    c-10.492,10.504-10.492,27.576,0,38.064L293.398,245.9l-184.06,184.06c-5.064,5.068-7.86,11.824-7.86,19.028
                                    c0,7.212,2.796,13.968,7.86,19.04l16.124,16.116c5.068,5.068,11.824,7.86,19.032,7.86s13.968-2.792,19.032-7.86L382.678,265
                                    c5.076-5.084,7.864-11.872,7.848-19.088C390.542,238.668,387.754,231.884,382.678,226.804z"/>
                        </svg>
                    </button>
                </div>
            </header>
            <ul className="items-list">
                { list && list.map((item: Item, index: number) => {
                    return(<ProjectListItem childRef={listRef} index={index} key={'project-list-item-' + index} item={item} years={periods} handleSelection={handleListSelect} />);
                })}
            </ul>
            { noResults && <div className="no-results">Ei leitud ühtegi vastet.</div>}
            <AddProjectItemModal show={show} projectId={project.projectId} handleClose={closeModal} />
            { showNotification && <Notification hide={hideNotification} title={notification.title} message={notification.message} />}
        </div>
    );
};

export default ProjectItemList;
import React from 'react';
import Period from '../../../interfaces/period';

interface Props {
    period: Period;
}

const ProjectItemPeriod: React.FC<Props> = ({period}) => {

    const formatPrice = (n: number) => {
        if (n < 1e3) return n;
        if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + "K";
        if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + "M";
        if (n >= 1e9 && n < 1e12) return +(n / 1e9).toFixed(1) + "B";
        if (n >= 1e12) return +(n / 1e12).toFixed(1) + "T";
      };

    return(
        <li>
            <div className="amount">{period.amount}</div>
            <div className="price">{formatPrice(period.price)}</div>
            <div className="sum">{formatPrice(period.price * period.amount)}</div>
        </li>
    );
};

export default ProjectItemPeriod;
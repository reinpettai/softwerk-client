import React, { useState, useEffect } from 'react';
import ProjectItemPeriod from '../ProjectItemPeriod/ProjectItemPeriod';
import Item from '../../../interfaces/item';

interface Props {
    childRef: any;
    item: Item;
    index: number;
    years: any[];
    handleSelection: (id: number, checked: boolean) => void;
}

const ProjectListItem: React.FC<Props> = ({ childRef, item, index, years, handleSelection }) => {

    const [selected, setSelected] = useState(false);
    const [yearItems, setYearItems] = useState<any[]>([]);
    const objectId = 'object'+item.itemId;
    const currentRef: any = React.useRef(null);
    const handleCheck = () => {
        if (selected) {
            setSelected(false);
            handleSelection(item.itemId, false);
            return;
        }
        setSelected(true);
        handleSelection(item.itemId, true);
    };

    useEffect(() => {

        setYearItems([]);

        years.forEach(year => {
        
            const yearItem = {
                year: year,
                period: {}
            };
    
            if (item && item.periods) {
                item.periods.map((m: any) => {
                    if (m.year === year) {
                        yearItem.period = m;
                    }
                    return [];
                });
            }
    
            setYearItems((prev: any) => ([...prev, yearItem]));
        });

    }, [item, years]);

    return(
        <li ref={ref => {
            childRef.current = ref;
            currentRef.current = ref;
        }} data-index={index} key={objectId} onClick={handleCheck} className={selected ? 'project-list-item-' + index + ' selected' : 'project-list-item-' + index}>
            <div className="code">{ item.itemId }</div>
            <div className="object">{ item.name }</div>
            <ul className="periods">
                { yearItems.map((item, i) => {

                    if (!item.period || !item.period.year) {
                        return (
                            <li key={'item-no-period-' + item.itemId + item.year + i}>
                                <div className="amount">-</div>
                                <div className="price">-</div>
                                <div className="sum">-</div>
                            </li>
                        );
                    }

                    return (<ProjectItemPeriod key={'item-period-' + item.period.periodId} period={item.period} />);
                    
                })}
            </ul>
        </li>
    );
}

export default ProjectListItem;
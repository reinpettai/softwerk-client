import React, { useState } from 'react';
import PeriodInput from './PeriodInput/PeriodInput';
import CloseButton from '../CloseButton/CloseButton';

interface Props {
    show: boolean;
    projectId: number;
    handleClose: (itemId: any) => void;
}

const AddProjectItemModal: React.FC<Props> = ({show, projectId, handleClose}) => {

    const [currentStep, setCurrentStep] = useState(0);
    const [objectName, setObjectName] = useState('');
    const [periodList, setPeriodList] = useState<any[]>([]);
    const [periodError, setPeriodError] = useState<boolean>(false);
    const [nameError, setNameError] = useState<boolean>(false);

    const closeModal = async () => {
        const itemId = await addObject();
        handleClose(itemId);
        setCurrentStep(0);
    };

    const periodInputHandler = async (period: any) => {

        setPeriodError(false);

        if (periodExists(period)) {
            return setPeriodError(true);
        }

        setPeriodList(prev => ([...prev, period]));
        setCurrentStep(2);
    };

    const setStep = (index: number) => {

        setNameError(false);

        if (index === 1 && objectName === '') {
            return setNameError(true);
        }
        setCurrentStep(index);
    };

    const periodExists = (period: any) => {
        
        const list = periodList.filter(obj => {
            return obj.year === period.year;
        });

        if (list.length > 0) return true;
        return false;
    };

    const addObject = async () => {

        if (currentStep !== 2) return;

        const body = {
            name: objectName,
            projectId: projectId,
            periods: periodList
        };

        const res = await fetch(process.env.REACT_APP_API_URL + 'items/add', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        });

        const content = await res.json();
        return content;

    };

    return(
        <div id="add-project-item-modal" className={show ? 'show' : ''}>
            <div className="background" onClick={closeModal}></div>
            <div className="inner">
                <CloseButton handleClick={closeModal} />
                { currentStep === 1 && 
                    <PeriodInput hasError={periodError} inputHandler={periodInputHandler} />
                }
                { currentStep === 0 &&
                <div className="object-name">
                    <h3>Lisa uus Hankeobjekt</h3>
                    <div className="input-container">
                        <label>Objekti nimi:</label>
                        <input type="text" onChange={event => setObjectName(event.target.value)} />
                        { nameError &&
                            <div className="name-error">Palun sisesta objekti nimetus</div>
                        }
                    </div>
                    <button onClick={() => setStep(1)}>Edasi</button>
                </div>
                }
                { currentStep === 2 && 
                <div className="final-step">
                    <p>Periood lisatud.</p>
                    <div className="actions">
                        <button className="add-new-period" onClick={() => setStep(1)}>Lisa uus periood</button>
                        <button className="finish" onClick={() => closeModal()}>Lisa hankeobjekt</button>
                    </div>
                </div>
                }
            </div>
        </div>
    );
};

export default AddProjectItemModal;
import React, { useState } from 'react';

interface Props {
    hasError: boolean;
    inputHandler: (period: any) => void;
}

const PeriodInput: React.FC<Props> = ({hasError, inputHandler}) => {

    const [inputError, setInputError] = useState<boolean>(false);
    const [period, setPeriod] = useState({
        year: '',
        price: '',
        amount: ''
    });
    
    const addHandler = () => {

        setInputError(false);

        if (!isInputValid()) {
            return setInputError(true);
        }
        inputHandler(period);
    };

    const isInputValid = () => {

        if (period.year !== '' && period.price !== '' && period.amount !== '') return true;
        return false;
    };

    return(
        <form className="add-period-input">
            <h3>Lisa periood</h3>
            <div className="input-container">
                <label>Aasta:</label>
                <input type="number" onChange={event => {
                    setPeriod({...period, year: event.target.value});
                }} />
            </div>
            <div className="input-container">
                <label>Hind:</label>
                <input type="number" onChange={event => {
                    setPeriod({...period, price: event.target.value});
                }} />
            </div>
            <div className="input-container">
                <label>Kogus:</label>
                <input type="number" onChange={event => {
                    setPeriod({...period, amount: event.target.value});
                }} />
            </div>
            { hasError && <div className="period-error">Sellise aastaga periood on juba lisatud.</div> }
            { inputError && <div className="input-error">Palun täida kõik väljad.</div> }
            <button onClick={(event) => {
                event.preventDefault();
                addHandler();
            }}>Lisa periood</button>
        </form>
    );
};

export default PeriodInput;
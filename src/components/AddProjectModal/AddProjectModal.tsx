import React, { useState } from 'react';
import CloseButton from '../CloseButton/CloseButton';

interface Props {
    show: boolean;
    handleClose: () => void;
    handleProjectAdded: (projectId: number) => void;
}

const AddProjectModal: React.FC<Props> = ({show, handleClose, handleProjectAdded}) => {

    const [projectName, setProjectName] = useState<string>();
    const [projectAdded, setProjectAdded] = useState<boolean>(false);
    const [error, setError] = useState<boolean>(false);
    const [nameError, setNameError] = useState<boolean>(false);

    const addProject = async () => {

        setError(false);
        setNameError(false);

        if (!projectName || projectName === '') {
            return setNameError(true);
        }

        const body = {
            name: projectName
        };

        try {
            const res = await fetch(process.env.REACT_APP_API_URL + 'projects/add', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            });

            const json = await res.json();
            
            if (json) {
                setProjectAdded(true);
                handleProjectAdded(json);
            }
            else {
                setError(true);
            }
        }
        catch(err) {
            setError(true);
        }
    };

    const close = () => {
        setProjectAdded(false);
        handleClose();
    };

    return(
        <div id="add-project-modal" className={show ? 'show' : ''}>
            <div className="background" onClick={close}></div>
            <div className="inner">
                <CloseButton handleClick={close} />
                { !projectAdded && 
                    <div className="input-container">
                        <h3>Lisa uus Hankekava</h3>
                        <label>Hankekava nimi:</label>
                        <input type="text" onChange={(event: any) => setProjectName(event.target.value)} />
                        { error && 
                            <div className="error">Viga projekti lisamisel.</div>
                        }
                        { nameError && 
                            <div className="error">Palun lisa hankekava nimetus.</div>
                        }
                    </div>
                }
                { projectAdded && 
                    <div className="project-added">
                        <p>Hankekava lisatud!</p>
                    </div>
                }
                { !projectAdded && <button onClick={addProject}>Lisa</button> }
            </div>
        </div>
    );
};

export default AddProjectModal;
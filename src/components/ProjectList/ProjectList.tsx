import React, { useState, useEffect, useRef } from 'react';
import AddProjectModal from '../AddProjectModal/AddProjectModal';
import Project from '../../interfaces/project';

interface ProjectListProps {
    handleSelect: (project: Project) => void;
}

const ProjectList: React.FC<ProjectListProps> = ({handleSelect}) => {

    const [projects, setProjects] = useState<any>([]);
    const [show, setShow] = useState(false);
    const [totalPages, setTotalPages] = useState<number>(1);

    let observer: any = useRef<IntersectionObserver>(null);
    let currentPage: any = useRef<number>(1);
    let listRef: any = useRef(null);

    const showModal = () => setShow(true);
    const closeModal = () => setShow(false);

    const abortController = new AbortController();

    const getData = async (page = 1) => {

        currentPage.current = page;

        try {
            const res = await fetch(process.env.REACT_APP_API_URL + 'projects/' + page, {signal: abortController.signal});
            const json = await res.json();

            if (page <= 1) {
                setTotalPages(json.result.totalPages);
                setProjects(json.result.items);
            }
            else setProjects([...projects, ...json.result.items]);
        }
        catch(err) {
            
        }
    };

    const handleProjectAdded = async (projectId: number) => {

        try {
            const res = await fetch(process.env.REACT_APP_API_URL + 'projects/id/' + projectId, {signal: abortController.signal});
            const json = await res.json();

            if (json && currentPage.current === totalPages) setProjects([...projects, json]);
        }
        catch(err) {
            
        }
    };

    useEffect(() => {

        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (observer && observer.current) observer.current.disconnect();
        if ((!listRef || !listRef.current) ||
            (totalPages === currentPage.current)) return;

        observer.current = new IntersectionObserver((entries: IntersectionObserverEntry[], observer: IntersectionObserverInit) => {
            const ratio = entries[0].intersectionRatio;

            if (ratio > 0) {
                const page = currentPage.current + 1;
                getData(page);
            }

        }, {});

        observer.current.observe(listRef.current);

        return () => {
            observer.current.disconnect();
            abortController.abort();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [projects, abortController]);

    return(
        <div className="project-list">
            <h2>Hankekavad</h2>
            <div className="actions">
                <button onClick={showModal}>Lisa uus Hankekava</button>
            </div>
            <header>
                <div className="code">Kood</div>
                <div className="object">Hankekava nimetus</div>
            </header>
            <ul>
                { projects.map((item: Project, index: number) => <li data-index={index} ref={listRef} onClick={() => { handleSelect(item) }} key={item.projectId != null ? 'project' + item.projectId : ''}><div className="code">{item.projectId}</div><div className="object">{item.name}</div></li>) }
            </ul>
            <AddProjectModal show={show} handleClose={closeModal} handleProjectAdded={handleProjectAdded} />
        </div>
    );
}

export default ProjectList;
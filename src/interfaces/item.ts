import Project from "./project";
import Period from "./period";

interface Item {
    itemId: number;
    name: string;
    projectId: number;
    project: Project;
    periods: Period[];
}

export default Item;
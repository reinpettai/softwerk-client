interface Project {
    projectId: number;
    name: string;
}

export default Project;
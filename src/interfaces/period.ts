interface Period {
    periodId?: number;
    itemId?: number;
    amount: number;
    price: number;
    year: number;
}

export default Period;
import React, { useState } from 'react';
import ProjectList from './components/ProjectList/ProjectList';
import BackButton from './components/BackButton/BackButton';
import ProjectItemList from './components/ProjectItemsList/ProjectItemsList';
import Project from './interfaces/project';

const App: React.FC = () => {

  const [selectedProject, setSelectedProject] = useState<Project>({projectId: 0, name: ''});

  const openProject = (project: Project) => {
    setSelectedProject(project);
  };

  const closeProject = () => {
    setSelectedProject({projectId: 0, name: ''});
  };

  return (
      <div className="app">
        { selectedProject.projectId > 0 && <BackButton handleClick={closeProject} />}
        
        { selectedProject.projectId <= 0 && <ProjectList handleSelect={openProject} /> }
        { selectedProject.projectId > 0 && <ProjectItemList project={selectedProject} /> }
      </div>
  );
}

export default App;
